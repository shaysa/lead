import { Component, OnInit } from '@angular/core';
import { PostsService } from './posts.service';

@Component({
  selector: 'app-posts',       //the name we will use in app.html to show the posts data
  templateUrl: './posts.component.html',
  //here we change the style to be with a hover affect 
  styles: [`                         
    .posts li { cursor: default; }
    .posts li:hover { background: #ecf0f1; } 
       .list-group-item.active, 
       .list-group-item.active:hover { 
        background-color: #ecf0f1;
        border-color: #ecf0f1; 
        color: #2c3e50;
    }  
  `]                             

})
export class PostsComponent implements OnInit {

  //  posts =[
  //      {author:'oz',book:'the old days',year:'1989'},
  //      {author:'sada',book:'hchim ke mashal',year:'1976'},
  //      {author:'oren',book:'night in parice',year:'1989'},

  //  ]
    // currentPost = this.posts[0];
    posts;
    currentPost;

    isLoading = true;

    

    select(post){
  	// this.currentPost = post; 

  // console.log(	this.currentPost);//here change
 }

 deletePost(post){
    // // this.posts.splice(
    // //   this.posts.indexOf(post),1  
    // )
    this._postsService.deletePost(post);
  }

addPost(post){
//Add a comment to this line
      // this.posts.push(post)
      // console.log(this.posts);
     this._postsService.addPost(post);


  }


editPost(post){
//Add a comment to this line
    // this.posts.splice(
    //   this.posts.indexOf(originalAndEdited[0]),1,originalAndEdited[1]  
    // )
    // console.log(this.posts);

    this._postsService.updatePost(post);
  }



  constructor(private _postsService: PostsService) {
    //this.posts = this._postService.getPosts();
   }

  ngOnInit() {
         
      this._postsService.getPosts()
      .subscribe(posts => {this.posts = posts;
                                this.isLoading = false;
                               console.log(posts)});
                              
		// 	    .subscribe(posts => this.posts = posts);
    // this._postsService.getPosts()
//Add a comment to this line
		    // .subscribe(postsData => {this.posts = postsData;
        //                       this.isLoading = false;
        //                     console.log(this.posts)});


  }

}
