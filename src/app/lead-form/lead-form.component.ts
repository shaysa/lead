import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import {NgForm} from '@angular/forms';
import {Lead} from '../lead/lead'


@Component({
  selector: 'jce-lead-form',
  templateUrl: './lead-form.component.html',
  styleUrls: ['./lead-form.component.css']
})
export class LeadFormComponent implements OnInit {

  @Output() leadAddedEvent = new EventEmitter<Lead>();
  lead:Lead = {
    id: '',
    sorce: ''
  };
  constructor() { }

 onSubmit(form:NgForm){
   
    console.log(form);
    this.leadAddedEvent.emit(this.lead);
    this.lead = {
       id: '',
       sorce: ''
    }
  }

  ngOnInit() {
  }

}
