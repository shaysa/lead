import { Component, OnInit , Output, EventEmitter} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Post} from '../post/post';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {

  @Output() postAddedEvent = new EventEmitter<Post>();
  post:Post = {
    title: '',
    body: ''
  };
  //usr:User = {name:'',email:''};  
   //post:Post = {title: '', body: ''}
 constructor() { }
   onSubmit(form:NgForm){
    console.log(form);
    //Add a comment to this line
   this.postAddedEvent.emit(this.post);
   console.log(this.post)
    this.post = {
       title: '',
       body: ''
    }
  }

  ngOnInit() {
  }

}
