import { Component, OnInit } from '@angular/core';
import {AngularFire} from 'angularfire2'; 
@Component({
  selector: 'app-fire',
  templateUrl: './fire.component.html',
  styleUrls: ['./fire.component.css']
})
export class FireComponent implements OnInit {

  //  posta;

  // constructor(af:AngularFire) { 
  //   af.database.list('/posta').subscribe(x => {this.posta = x;
  //     console.log(this.posta)})
  // } 

     leads;

  constructor(af:AngularFire) { 
    af.database.list('/leads').subscribe(x => {this.leads = x;
      console.log(this.leads)})
  } 

  ngOnInit() {
  }

}
