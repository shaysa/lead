import { Component, OnInit , Output, EventEmitter} from '@angular/core';
import {Lead} from './lead'
@Component({
  selector: 'jce-lead',
  templateUrl: './lead.component.html',
  styleUrls: ['./lead.component.css'],
  inputs:['lead']
})
export class LeadComponent implements OnInit {

@Output() deleteEvent = new EventEmitter<Lead>();
@Output() editEvent = new EventEmitter<Lead>();
   lead:Lead;
   templead:Lead = {id:null,sorce:null};
  isEdit : boolean = false;
  editButtonText = 'Edit';

  constructor() { }

  sendDelete(){
    this.deleteEvent.emit(this.lead);
  }

  cancelEdit(){
    this.isEdit = false;
// Add a comment to this line
    this.lead.id = this.templead.id;
    this.lead.sorce = this.templead.sorce;
    this.editButtonText = 'Edit'; 
  }

  toggleEdit(){
     //update parent about the change
     this.isEdit = !this.isEdit; 
     this.isEdit ?  this.editButtonText = 'Save' : this.editButtonText = 'Edit'; 
      if(this.isEdit){
       this.templead.id = this.lead.id;
       this.templead.sorce = this.lead.sorce;
      } else {
      
       this.editEvent.emit(this.lead);
     }   
  }


  ngOnInit() {
  }

}
