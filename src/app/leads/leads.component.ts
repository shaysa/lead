import { Component, OnInit } from '@angular/core';
import { LeadsService } from './leads.service';

@Component({
  selector: 'jce-leads',
  templateUrl: './leads.component.html',
styles: [`
    .leads li { cursor: default; }
    .leads li:hover { background: #ecf0f1; } 
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }  
  `]
})
export class LeadsComponent implements OnInit {

  leads;

          currentLead;
// adding the loding affect
          isLoading = true;

   constructor(private _leadsService: LeadsService) {
    this.leads = this._leadsService.getLeads();
   }

          // this is for selectinng a spesific lead 

          
   select(lead){

		this.currentLead = lead; 
   // console.log(	 this.currentLead);  can be needed 
 }
    deleteLead(lead){
    //  this.leads.splice(
    //  this.leads.indexOf(lead),1
   // )
    this._leadsService.deleteLeads(lead);
    
  }

addLead(lead){

  this._leadsService.addleads(lead);
}

  editLead(lead)
  {

    this._leadsService.updateLead(lead);
  }
    

// calling getleads func frome sirves.ts wher the data sites 
 

  ngOnInit() {
// this._leadsService.getLeads()
//        .subscribe(users => {this.leads = leads;
//      this.isLoading = false;
//            console.log(leads)});

// this._leadsService.getLeads().subscribe(leadsData => this.leads = leadsData);
  }

}
