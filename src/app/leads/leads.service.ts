import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import {AngularFire} from 'angularfire2'; 
import{AngularFireModule} from 'angularfire2';
// import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';

@Injectable()
export class LeadsService {

 // private _url = "http://shaysa.myweb.jce.ac.il/distributed/api/users/";
   leads = [

            {id: '001', sorce : 'phone'},
            {id: '002', sorce : 'web'},
            {id: '003', sorce : 'add'}

           ]


           leadsObservable;

           getLeads(){
          this.leadsObservable = this.af.database.list('/leads');  //השורה הזאת מזינה נתונים לפיירבייס המתאים 
        // return this.leadsObservable;
             
             return this.leads;
           }

           deleteLeads(lead){
           this.af.database.object('/leads/' + lead.$key).remove();
          console.log('/leads/' + lead.$key)
           }
           addleads(lead){
             this.leadsObservable.push(lead);

           }
            updateLead(lead){
       let lead1 = {id:lead.id,sorce:lead.sorce}
       console.log(lead1);
      this.af.database.object('/leads/' + lead.$key).update(lead1)
    
   }

  constructor(private  af:AngularFire) { }

}
