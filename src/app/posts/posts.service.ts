import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import {AngularFire} from 'angularfire2'; 
import{AngularFireModule} from 'angularfire2';
// import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';

@Injectable()
export class PostsService {



   private _url = "http://jsonplaceholder.typicode.com/posts";
    // private_url = "http://shaysa.myweb.jce.ac.il/api/users.typicode.com/posts"; 22.01
   postsObservable;
   constructor(private  af:AngularFire) { }
    //  constructor(private  af:AngularFire, private _http:Http) { } 22.01



    // getUsersFromApi(){                                              22.01
    //   return this._http.get(this._url).map(res =>res.jason());
    // }

   addPost(post){
    this.postsObservable.push(post);
  }

   deletePost(post){
   this.af.database.object('/posts/' + post.$key).remove();
   console.log('/posts/' + post.$key)

   }

   updatePost(post){
       let post1 = {title:post.title,body:post.body}
    console.log(post1);
    this.af.database.object('/posts/' + post.$key).update(post1)
    
   }

  //  getPosts(){

  //    this.postsObservable = this.af.database.list('/posts');
  //    return this.postsObservable;
  //  }


// getPosts hase to have a method in order to show in the browser
    // getPosts(){
    //   //   return this.posts;
    //       return this._http.get(this._url)
		//        	.map(res => res.json()).delay(3000);
    // }
// this needs to be chect גם פה צריך לראות מה הסיפור של פונקצית ההחזרה של הנתונים 
     getPosts(){
    this.postsObservable = this.af.database.list('/posts').map(
      posts =>{
        posts.map(
          post => {
            post.posUsers = [];
            for(var p in post.users){
                post.posUsers.push(
                this.af.database.object('/users/' + p)
              )
            }
          }
        );
        return posts;
      }
    )
    //this.usersObservable = this.af.database.list('/users');
    return this.postsObservable;
	}

 // constructor() { }

}
